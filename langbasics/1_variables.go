package main

import (
	"fmt"
	"math"
)

/* It is an error to import a package or to declare a variable without using it. */

func Run1() {

	//String concatenation
	fmt.Println("go" + "lang")

	//Numerics
	fmt.Println("1+1 =", 1+1)
	fmt.Println("7.0/3.0 =", 7.0/3.0)

	//bool
	fmt.Println(true && false)
	fmt.Println(true || false)
	fmt.Println(!true)

	/* Variables and Contants*/

	//declaration
	var a = "init"      //var declares 1 or more variables.
	var b, c int = 1, 2 //declare multiple variables with type

	//Variables declared without a corresponding initialization are zero-valued.
	//For example, the zero value for an int is 0, string is "".
	var x int    //0
	var y string //"" Note the blank (newline) while printing, between x and z, i.e 0 and false
	var z bool   // false

	println(a, b, c) //println is an utility function from  pkg.go.dev and ittakes multiple arguments
	println(x)
	println(y)
	println(z)

	//Declartion and initialization at the same time
	f := "thisisme" //same as var f string = "thisisme"
	println(f)

	/* Redeclaration and reassignment

	In a := declaration a variable v may appear even if it has already been declared, provided:
	- this declaration is in the same scope as the existing declaration of v (if v is already declared in an outer scope, the declaration will create a new variable §),
	- the corresponding value in the initialization is assignable to v, and
	- there is at least one other variable that is created by the declaration.
	*/

	one := func ()(int, string) {
		return 1, "one"
	}
	two := func() (int, string) {
		return 2, "two"
	}

	g, h := one()
	println(g, h)
	g, k := two()
	println(g, k)

	l, m := 1, 2
	println(l, m)
	l, n := 3, 4
	println(l, n)

	//p := 1
	//p := 2 // doesn't work as condition "there is at least one other variable that is created by the declaration"

	//Constants
	const o = 500000000
	const d = 3e20 / o //arbitrary precision.
	println(d)
	println(int64(d)) //A numeric constant has no type until it’s given one, such as by an explicit conversion.

	//A number can be given a type by using it in a context that requires one,
	// such as a variable assignment or function call.
	//For example, here math.Sin expects a float64.
	fl := math.Sin(o)
	println(fl)
}
