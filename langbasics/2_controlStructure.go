package main

import (
	"fmt"
	"time"
)

func Run2() {

	/* For */
	//Simple for, like if
	i := 1
	for i <= 3 {
		println(i)
		i++
	}

	//classic
	for j := 7; j <= 9; j++ {
		println(j)
	}

	//without condition, will work till break
	for {
		println("loop")
		break
	}

	//Continue
	for n := 0; n <= 5; n++ {
		if n%2 == 0 {
			continue
		}
		println(n)
	}

	//Iterate over string
	for pos, char := range "日本\x80語" { // \x80 is an illegal UTF-8 encoding
		fmt.Printf("character %#U starts at byte position %d\n", char, pos)
	}

	/* If else */ //There is no ternary

	if 7%2 == 0 {
		println("even")
	} else {
		println("odd")
	}

	//A variable can be instatntiated and accessed inside the loop
	if num := 9; num < 0 {
		println(num, "is negative")
	} else if num < 10 {
		println(num, "has 1 digit")
	} else {
		println(num, "has multiple digits")
	}

	/* Switch */
	//Simple
	a := 2
	switch a {
	case 1:
		println("one")
	case 2:
		println("two")
	case 3:
		println("three")
	}

	//With default and multiple match
	switch time.Now().Weekday() {
	case time.Saturday, time.Sunday:
		fmt.Println("weekend")
	default:
		fmt.Println("weekday")
	}

	//Without expression, works like if else
	t := time.Now()
	switch {
	case t.Hour() < 12:
		println("AM")
	default:
		println("PM")
	}

	// A type switch matches type instead of values
	decodeType := func(i interface{}) {
		switch t := i.(type) {
		case bool:
			println("Boolean")
		case int:
			println("Integer")
		default:
			fmt.Printf("Other type %T\n", t)
		}
	}
	decodeType(true)
	decodeType(1)
	decodeType("hey")
	decodeType(decodeType)
}
