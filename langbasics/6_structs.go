package main

import (
	"fmt"
	"math"
)

/*	- Structs are type collection of fields to represent records
*	- Methods are functions on struct type. Methods can be defined for either pointer or value receiver types
 */
func Run6() {

	/* Struct Creation*/

	//Plain creation
	p := person{"Adi", 16}
	fmt.Println(p)
	p.age = 14 //mutable
	fmt.Println(p)

	//with constructor function
	pv := newPersonVal("Villi")
	fmt.Println(pv)

	pp := newPersonPntr(50)
	fmt.Println(*pp)
	fmt.Println(pp.age) //automatic derefrencing, NOT applicable for pp itself
	fmt.Println(pp)     //Prints the address
	pp.age = 60         //Mutable wit auto dereferencing too
	fmt.Println(*pp)

	p0 := person{name: "Adam"}
	fmt.Println(p0) //blank parameters are zero valued

	/* Methods*/
	//A pointer receiver type may be used to avoid copying on method calls
	//or to allow the method to mutate the receiving struct

	pM := person{"Jhinku", 12}
	fmt.Println(*pM.addAge(4))      //pointer receiver - pointer return, changes the underlaying struct, * is required to print value, it is as same as *(pM.addAge(4))
	fmt.Println(pM.addSalutation()) //value receiver, DOES NOT change the underlaying struct
	println(pM.name)                // Prints "Jhinku". Name is not changed as it is a value reciever

	//calling with pointers
	pmp := &pM
	fmt.Println(*pmp.addAge(6))        //pointer receiver,the age is now 12+4+6 - 22, so salutation is master
	fmt.Println((&pM).addSalutation()) // The barcket around &pM is required otherwise it will not compile

	fmt.Println(pM.subtractAge(5)) //pointer receiver but value return, it is easier to work and less error prone
	fmt.Println(pM.addSalutation())

	/* Interfaces - named collections of method signature
	*	To implement an interface you just need to ovverride all the methods of interface
	 */

	c := circle{rad: 3}
	r := rectangle{3, 4}
	showMeasures(c)
	showMeasures(r)

}

// Declaring a struct
type person struct {
	name string
	age  int
}

// Declaring an interface
type shape interface {
	area() float64
	perim() float64
}

type circle struct {
	rad float64
}

type rectangle struct {
	height, width float64
}

//	Overriding the rectangle methods
func (r rectangle) area() float64 {
	return r.height * r.width
}

func (r rectangle) perim() float64 {
	return 2 * (r.height + r.width)
}

//	Overriding the circle methods
func (c circle) area() float64 {
	return math.Pi * math.Pow(c.rad, 2)
}

func (c circle) perim() float64 {
	return 2 * math.Pi * c.rad
}

// With a variable typeof interface we can call the methods
func showMeasures(s shape) {
	fmt.Println(s)
	fmt.Println(s.area())
	fmt.Println(s.perim())
}

// It is idiomatic to use constructs to create new structs

func newPerson(name string, age int) person {

	p := person{age: age, name: name} //notice the use of named parameter
	return p                          // Its safe to return pointer, automatic dereferencing
}

func newPersonVal(name string) person {

	p := person{name: name}
	p.age = 20
	return p
}

func newPersonPntr(age int) *person {

	p := person{age: age} //notice the use of named parameter
	p.name = "John Doe"   // Local variable will survive as it is returning a pointer
	return &p             // Its safe to return pointer, automatic dereferencing
}

func (p person) addSalutation() string {

	//automatic dereferencing
	if p.age < 18 {
		p.name = "Master " + p.name
	} else {
		p.name = "Mr.  " + p.name
	}
	return p.name
}

func (p *person) addAge(val int) *person {
	p.age += val
	return p
}

func (p *person) subtractAge(val int) person {
	p.age -= val
	return *p
}
