package main

import "fmt"

func Run3() {
	/* Arrays */
	//declaration with length and type, intitialed with zero values
	var ar1 [5]int
	var ar2 [3]bool
	fmt.Println(ar1)
	fmt.Println(ar2)

	//value assignment
	ar1[2] = 400
	fmt.Println(ar1)

	//len returns length of the array
	println(len(ar1))

	//declare and initialize
	ar3 := [5]int{1, 2, 3, 4, 5}
	fmt.Println(ar3)

	//declare and initialize - without explicitly declaing size
	ar4 := [...]string   {"str1", "str2", "Str3", "str4"}
	fmt.Println(ar4)

	//Multi dimentional
	var arMd [5][4][3]int
	for i := 0; i < 5; i++ {
		for j := 0; j < 4; j++ {
			for k := 0; k < 3; k++ {
				arMd[i][j][k] = i + j + k
			}
		}
	}
	fmt.Println(arMd)

	/* Slices - Just like arrays, dynamically increasing, but more used */
	//declaration with initial range of 5
	sl1 := make([]string, 3)
	sl1[0] = "a"
	sl1[2] = "c"
	fmt.Println(sl1)

	//from array
	ar := [4]int{1, 2, 3, 4}
	sl := ar[1:3]
	fmt.Println(sl)

	//declare and initialize
	slc := []string{"x", "y", "z"}
	fmt.Println(slc)

	//append - have to accept a new return value
	sl1 = append(sl1, "d")
	fmt.Println(sl1)

	//len and cap
	println(len(sl1))
	println(cap(sl1))

	sl1 = append(sl1, "d", "e", "f", "g", "h")

	//len and cap
	fmt.Println(sl1)
	println(len(sl1))
	println(cap(sl1))

	//copy
	sl2 := make([]string, len(sl1))
	copy(sl2, sl1)
	fmt.Println(sl2)

	//slicing
	fmt.Println(sl1[2:5], sl1[:5], sl1[2:])

	//iterating
	//classic loop, index based
	for i := 0; i < len(sl1); i++ {
		print(sl1[i])
	}
	println("")
	//with range
	for index, element := range sl1 {
		fmt.Printf("Index = %d , Element = %s\n", index, element)
	}

	//ignoring index
	for _, element := range sl1 {
		fmt.Printf("Element = %s\n", element)
	}

	/* THE BLANK IDENTIFIER: _ */
	//	The blank identifier can be assigned or declared with any value of 
	//	any type, with the value discarded harmlessly

	//ignoring element
	for index := range sl1 {
		fmt.Printf("Index = %d \n", index)
	}

	//multi dimentional - As arrays

	/* Maps */
	//Create
	mp := make(map[string]int)

	mp["k1"] = 10
	mp["k2"] = 20

	fmt.Println(mp)

	//Declaration and initialization
	mp2 := map[string]int{"k3": 30, "k4": 40, "k5": 50}
	fmt.Println(mp2)

	//delete by key
	delete(mp, "k2")
	fmt.Println(mp)

	//iteration
	for k, v := range mp2 {
		fmt.Printf("Key = %s, Value = %d\n", k, v)
	}

	//only key
	for k := range mp2 {
		fmt.Printf("Key = %s\n", k)
	}

	//Whether a key is present or not - Very usefull feature
	value, isPresent := mp2["k4"]
	println(value, isPresent)

	//ignoring the value
	_, isPrsnt := mp2["non_present_key"]
	println(isPrsnt)

	/* Range - apart from array, slice and map*/
	//unicode code points
	for i, c := range "apple" {
		println(i, c)
	}

}
