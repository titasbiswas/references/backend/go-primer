package main

import "fmt"

// init() - each source file can define its own niladic init function to set up whatever state is required
//init is called after all the variable declarations in the package have evaluated their initializers
func init() {
	fmt.Println("in the init function")
}

func Run7() {
	/*	NEW and MAKE
	*	Go has two allocation primitives, the built-in functions new and make
	 */

	/*	NEW allocates memory but doesn't initialize it.
		*	new(T) allocates zeroed storage for a new item of type T and returns its address, a value of type *T
		it returns a pointer to a newly allocated zero value of type T.
	*/

	var p = new(Person) // OR p := new(Person)
	fmt.Println("Zerod person", *p, p.age, p.name)
	p.age = 21
	p.name = "John Cena"
	fmt.Println("With data", p.age, p.name)

	//Same as
	p2 := &Person{name: "Ali baba", age: 40}
	fmt.Println("With composite literals", p2.age, p2.name)

	/*	MAKE  It creates slices, maps, and channels only.
	*	It returns an initialized (not zeroed) value of type T (not *T)
	 */

	nPtr := new([]Person) // Allocates nil pointer, not very usefull
	fmt.Println("Nil pointer", nPtr, *nPtr == nil)

	//	Allocates an array of 100 Persons and then creates a slice structure
	//	with length 5 and a capacity of 100 pointing at the first 5
	//	The ements are itself zeroed elements of the array.
	slc := make([]Person, 5, 100)
	fmt.Println(slc)

	// APPEND
	x := []int{1, 2, 3}
	x = append(x, 4, 5, 6)
	fmt.Println(x)

	//append with spread
	xx := []int{1, 2, 3}
	yy := []int{4, 5, 6}
	xx = append(xx, yy...)
	fmt.Println(xx)

	// ENUM

	m := TWENTIES
	fmt.Println(m)

}

// Decalring enum type
type Mult int

const (
	_         = iota
	TENS Mult = iota * 10
	TWENTIES
	THIRTIES
	FORTIES
	FIFTIES
)

func (m Mult) String() string {
	switch m {
	case TENS:
		return "Multiplied by 10"
	case TWENTIES:
		return "Multiplied by 20"
	case THIRTIES:
		return "Multiplied by 30"
	case FORTIES:
		return "Multiplied by 40"
	case FIFTIES:
		return "Multiplied by 50"
	default:
		return "Not a valid multiplier"
	}
}

type Person struct {
	age  int
	name string
}
