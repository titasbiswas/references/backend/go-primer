package main

import (
	"fmt"
	"time"
)

func Run4() {
	/* Functions */
	//general, with onw time declaration if all the params of same type
	println(tripSum(1, 2, 3))

	// MULTIPLE RETURN VALUE
	i, s := multReturn()
	println(i, s)

	//NAMED return parameters - see the func declaration
	in, st := namedReturn()
	fmt.Printf("Int result = %d, String Result = %s\n", in, st)

	//DEFER function, called once the encoled func call is over
	defFuncCaller()

	// Defer works in LIFO
	for i := 0; i < 5; i++ {
		defer fmt.Printf("%d ", i)
	}

	//OR

	defer deferLifo(1)
	defer deferLifo(2)

	//Subset of the return, ignoring some returns
	_, l, _, g := loveGo()
	println(l, g)

	/* Variadic Functions - any number of trailing argument*/
	println(vrdSum(3, 4, 5))
	nums := []int{6, 8, 9, 10}
	println(vrdSum(nums...)) //spread the slice

	/* Closures */
	//Go supports anonymus functions which can form closures.
	/*
		Wiki: The environment is a mapping associating each free variable of the function
		(variables that are used locally, but defined in an enclosing scope) with the value
		or reference to which the name was bound when the closure was created.
	*/

	mahAdder := superAdder(10) //Initiate the clousre with 10, the scope is only in mahAdder

	println(mahAdder(2)) //10 + (2), i is 12 now
	println(mahAdder(3)) //12 + (3), i is 15 now
	println(mahAdder(5)) // 15 + (5)

	mahNextAdder := superAdder(3) //mahNextAdder has an initial (closure) value of i as 3
	println(mahNextAdder(2))      //2 + (3)

	sqFunc := func(j int) int {
		return j * j
	}
	mahFunc := superFunc(5, sqFunc)
	println(mahFunc)

	/* Recursion */
	//Simple factorial example
	println(fact(4)) //24

}

func deferLifo(i int){
fmt.Printf("In defer lifo test %d\n", i)
}

func defFuncCaller() {
	i := 1
	defer defFunc(i) //The arguments to the deferred function (which include the receiver if the function is a method) are evaluated when the defer executes, not when the call executes.
	for j := 2; j < 6; j++ {
		i++
	}
	fmt.Printf("Value at the end of the defered calling func function %d\n", i)

}

func defFunc(i int) {
	fmt.Printf("Value in defered function %d\n", i)
}

func fact(n int) int {
	if n == 0 {
		return 1
	}
	return n * fact(n-1)
}

func superFunc(i int, f func(j int) int) int {
	return f(i)
}

func superAdder(i int) func(int) int {
	return func(j int) int {
		i = i + j
		return i
	}
}

func tripSum(a, b, c int) int {
	return a + b + c
}

func multReturn() (int, string) {
	return 10, "Ten"
}

func namedReturn() (intResult int, strResult string) { //When named, they are initialized to the zero values for their types when the function begins

	intResult = time.Now().Second()

	return // if the function executes a return statement with no arguments, the current values of the result parameters are used as the returned values.
}

func loveGo() (string, string, string, string) {
	return "I", "love", "to", "GO"
}

func vrdSum(nums ...int) int {
	sum := 0
	for _, num := range nums { //Ignoring the index with _ is necessary, otherwise the loop will end up suming up indices
		sum += num
	}
	return sum
}
