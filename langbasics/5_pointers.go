package main

/*
The rule about pointers vs. values for receivers is that value methods can be invoked on pointers and values, 
but pointer methods can only be invoked on pointers.
There is a handy exception, though. When the value is addressable, 
the language takes care of the common case of invoking a pointer method on a value by inserting the address operator automatically.
*/

func Run5() {
	i := 1
	println(i)

	incVal(i)
	println(i)

	//As a pointer the memoryspace address of the variable is passed
	//we can access the memoryspace with &<varname>
	incPntr(&i)
	println(i)
	println(&i) // printing the pointer itself, i.e. the memory address


}

//Declare a pointer lik <pointer_varname> *<var_type>
func incPntr(iptr *int) {
	//DEREFERENCING: access a POINTER VALUE like *<pointer_varname>
	*iptr++
}

func incVal(ival int) {
	ival++
}
