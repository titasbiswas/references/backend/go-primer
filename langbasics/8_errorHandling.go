package main

import (
	"errors"
	"fmt"
)

func Run8() {
	//	Simple error
	_, err:= simpleError()
	if err != nil{
		fmt.Println(err)
	} 
	empStore := make([]employee, 0)
	emp, err := savePerson(&employee{name: "Haley", age: 17}, &empStore)

	if err != nil {
		fmt.Println(err) // Simple logging
		// To access error data programmatically you need type assertion
		if errData, hasError := err.(*empSaveErr); hasError {
			fmt.Println(errData.arg)
			fmt.Println(errData.msg)
		}

		//Multiple known error
		switch errType := err.(type) {
		case *empSaveErr:{
				fmt.Println(errType.arg)
				fmt.Println(errType.msg)
			}
		default:{
				fmt.Print("Unknon error type", err)
			}
		}
	}

	fmt.Println(emp)

}

func simpleError()(result int, err error){
	return 0, errors.New("Error! Error! Error! ")
}

type employee struct {
	id, age int
	name    string
}

func savePerson(emp *employee, empStore *[]employee) (employee, error) {
	if emp.age < 18 {
		return employee{}, &empSaveErr{msg: "Age must be more than", arg: "18"}
	}
	//employees := make([]employee, 0)
	emp.id = len(*empStore)
	*empStore = append(*empStore, *emp)
	return *emp, nil
}

//	Declaring the custom error type
type empSaveErr struct {
	arg, msg string
}

//	To implement custom types as error you need to implement Error() method on them
func (e *empSaveErr) Error() string {
	return fmt.Sprintf("%s %s", e.msg, e.arg)
}
